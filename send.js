const express = require ("express");
const app = express();
const ampq = require('amqplib/callback_api');
const logger = require('./logger')

const port = 3001;


    ampq.connect('amqp://localhost',(err,conn)=>{
    conn.createChannel((err,ch)=>{
        var queue = "credqueue";
        var message = { context: 'Hello RabbitMQ' };
        ch.assertQueue(queue,{durable :false});
        ch.sendToQueue(queue,Buffer.from(JSON.stringify(message)));
        logger.log ("info","Message was sent");
    });

    setTimeout(()=>{
        conn.close();
        process.exit(0);
    },3000);
})




app.listen(port,()=>{
    console.log(`Listening on port ${port}`);
})