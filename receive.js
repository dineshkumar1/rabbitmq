const express = require ("express");
const app = express();
const ampq = require('amqplib/callback_api');
const logger = require('./logger')

const port = 3002;

ampq.connect('amqp://localhost',(err,conn)=>{
    conn.createChannel((err,ch)=>{
        var queue = "credqueue";
        ch.assertQueue(queue,{durable :false});
        console.log(`Waiting for message in ${queue}`);
        ch.consume(queue,(message)=>{
        logger.log("info",`Received ${message.content}`);
        },{noAck : true});
    });
})

app.listen(port,()=>{
    console.log(`Listening on port ${port}`);
})